class LazyAttr:
    def __init__(self, factory):
        self._name = None
        self.__factory__ = factory

    @property
    def name(self):
        return self._name

    def __set_name__(self, owner, name):
        assert self.name is None
        self._name = name

    def __get__(self, instance, owner=None):
        if instance is None:
            assert owner is not None
            return self
        dct = vars(instance)
        try:
            value = dct[self.name]
        except KeyError:
            value = dct[self.name] = self.__factory__(instance)
        return value

    def __set__(self, instance, value):
        raise AttributeError(f"can't set attribute {self.name!r}")

    def __delete__(self, instance):
        del vars(instance)[self.name]

    def initialized(self, instance):
        return self.name in vars(instance)

    def get(self, instance):
        if not self.initialized(instance):
            name = type(instance).__name__
            raise AttributeError(
                f"attribute {self.name!r} of {name!r} object not initialized"
            )
        return self.__get__(instance)

    @classmethod
    def collect_all(cls, obj):
        "iterates over all descriptors of obj"
        cls_attrs = vars(type(obj)).values()
        return (descr for descr in cls_attrs if isinstance(descr, cls))

    @classmethod
    def collect(cls, obj):
        "iterates over all initialized descriptors of obj"
        attrs = cls.collect_all(obj)
        return (d for d in attrs if d.initialized(obj))
