class MultConverter:
    "object that applies a function using multiply operator"

    def __init__(self, func):
        self.func = func

    def __mul__(self, other):
        return self.func(other)

    def __rmul__(self, other):
        return self.func(other)


#####################################################################
## SI base units

# the 7 base units
second = 1  # second
m = 1  # meter
kg = 1  # kilogram
A = 1  # ampere
K = 1  # kelvin
mol = 1  # mole
cd = 1  # candela

# second variants
minute = 60 * second
hour = 60 * minute
day = 24 * hour
week = 7 * day
year = 365.25 * day
ky = 1e3 * year
My = 1e6 * year

# meter variants
mm = 1e-3 * m
cm = 1e-2 * m
km = 1e3 * m

# kg variants
g = 1e-3 * kg
ton = 1000 * kg

# kelvin variants
degC = MultConverter(lambda T: T + 273.15)

#####################################################################
## Some named units derived from SI base units

# pascal
Pa = kg * m**-1 * second**-2
MPa = 1e6 * Pa
bar = 1e5 * Pa

# joule
J = kg * m**2 * second**-2
kJ = 1e3 * J

# watt
W = J / second
kW = 1e3 * W

#####################################################################
## utils


def time_string_factory(time_unit, symbol):
    def f(tin):
        return f"{tin:10.5g} s = {tin / time_unit:10.5g} {symbol}"

    return f


time_string = time_string_factory(year, "y")
