#
# This file is part of ComPASS.
#
# ComPASS is free software: you can redistribute it and/or modify it under both the terms
# of the GNU General Public License version 3 (https://www.gnu.org/licenses/gpl.html),
# and the CeCILL License Agreement version 2.1 (http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html).
#

import numpy as np


def tensor_coordinates(tensor, name, diagonal_only=False):
    """
    Creates dictionary intended to be written to vtu as an array with numberofcomponents = 9
    Can be visualized in paraview with the following procedure
    - create a Cell Centers filter as a child of the whole grid or a filter thereof
    - create a Tensor Glyph filter as child of the Cell Centers
    - in the Tensor Glyph filter's properties, select the property among the Tensors chooser
    - adjust radius
    """
    tensor = np.asarray(tensor)
    assert tensor.ndim == 2 or tensor.ndim == 3, "wrong tensor array dimension"
    assert tensor.shape[-1] == tensor.shape[-2]
    dim = tensor.shape[-1]
    assert dim <= 3, "dimension should be 3 at max"
    if diagonal_only:
        return {
            f"{name}{si}{si}": tensor[..., i, i] for i, si in enumerate("xyz"[:dim])
        }
    if tensor.ndim == 3:
        tensor = tensor.reshape([tensor.shape[0], tensor.shape[1] * tensor.shape[2]])
    return {name: tensor}
