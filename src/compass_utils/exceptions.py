class LinearSolverFailure(Exception):
    def __init__(self, reason):
        self.reason = reason

    def __str__(self):
        return f"Linear solver failed with reason : {self.reason}"
