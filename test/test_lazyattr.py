import pytest

from compass_utils.lazyattr import LazyAttr


def test_decorator():
    class A:
        @LazyAttr
        def x(self):
            return [42]

    _test_Ax(A)


def test_affectation():
    class A:
        x = LazyAttr(lambda s: [42])

    _test_Ax(A)


def _test_Ax(A):

    a = A()
    assert vars(a) == {}
    assert a.x == [42]
    assert vars(a) == {"x": [42]}
    a.x[0] = 12
    assert a.x == [12]

    with pytest.raises(AttributeError):
        a.x = "nope"

    assert {d.name: d.get(a) for d in LazyAttr.collect(a)} == {"x": [12]}

    del a.x
    assert vars(a) == {}
    assert a.x == [42]
    assert vars(a) == {"x": [42]}

    A.x.__delete__(a)
    assert vars(a) == {}
    assert a.x == [42]
    assert vars(a) == {"x": [42]}


def test_custom_attr():
    class FixedSize(LazyAttr):
        def __init__(self, size):
            super().__init__(lambda s: [0] * size)

    class AdaptiveSize(LazyAttr):
        def __init__(self, size_name):
            super().__init__(lambda s: [0] * getattr(s, size_name))

    class A:
        N1 = 3
        N2 = 4
        x = FixedSize(5)
        y1 = AdaptiveSize("N1")
        y2 = AdaptiveSize("N2")

    a = A()
    assert a.x == [0, 0, 0, 0, 0]
    assert a.x is vars(a)["x"]
    assert a.y1 == [0, 0, 0]
    assert a.y1 is vars(a)["y1"]
    assert a.y2 == [0, 0, 0, 0]
    assert a.y2 is vars(a)["y2"]

    assert {d.name: len(d.get(a)) for d in LazyAttr.collect(a)} == {
        "x": 5,
        "y1": 3,
        "y2": 4,
    }
    assert {d.name: len(d.get(a)) for d in FixedSize.collect(a)} == {
        "x": 5,
    }
    assert {d.name: len(d.get(a)) for d in AdaptiveSize.collect(a)} == {
        "y1": 3,
        "y2": 4,
    }

    b = A()
    assert [d.name for d in LazyAttr.collect(b)] == []
    b.N1 = 1
    assert b.y1 == [0]
    assert [d.name for d in LazyAttr.collect(b)] == ["y1"]
    assert b.y2 == [0, 0, 0, 0]
    assert [d.name for d in LazyAttr.collect(b)] == ["y1", "y2"]
    assert b.x == [0, 0, 0, 0, 0]
    assert [d.name for d in LazyAttr.collect(b)] == ["x", "y1", "y2"]
